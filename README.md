# Profile Page of Irina Ivanova
**NB! This is an old page! New one is https://irina-ivanova.eu and is hosted on GitHub: https://github.com/iriiiina/profile-page**

## Technologies Used
* HTML5
* CSS3 ([Responsive Design](http://www.w3schools.com/css/css_rwd_intro.asp))
* AngularJS (show last 3 posts from Blogger)
* TeX (CV files)
* Git
* W3C HTML and CSS validators

## Graphical Elements
* [Flat Design Colors](http://www.flatdesigncolors.com)
* [Unicode Characters](https://unicode-table.com)
* [Font Awesome](http://fontawesome.io)
* [Favicon](http://www.favicon.cc)

## Performance Optimization
* [Image Oprimization](https://developers.google.com/web/fundamentals/performance/optimizing-content-efficiency/image-optimization)
* [Responsive Break Points](http://www.responsivebreakpoints.com)
* [HTML Minifier API](http://html-minifier.com)
* [CSS Minifier API](https://cssminifier.com)
* [JavaScript Minifier API](https://javascript-minifier.com)

## Project Management
* GitLab repo
* GitLab Issue Manager
* [GitLab Continues Integration](https://about.gitlab.com/gitlab-ci)
* [SEO](https://support.google.com/webmasters#topic=3309469)
* [A Guide to Sharing for Webmasters](https://developers.facebook.com/docs/sharing/webmasters)

## Blogger API
* [Blogger API Introduction](https://developers.google.com/blogger/)

## Hosting the Page on GitLab
* [Getting Started with Pages](https://pages.gitlab.io)
* [GitLab Pages Setup](https://about.gitlab.com/2016/04/07/gitlab-pages-setup)
* [Documentation about Pages](https://docs.gitlab.com/ee/pages/README.html)

## Blog Posts
* [Profile Page with HTML5 and CSS3](https://ivanova-irina.blogspot.com.ee/2017/01/profile-page-with-html5-and-css3.html)
* [Profile Page 2.0](https://ivanova-irina.blogspot.com.ee/2017/02/profile-page-20.html)
* [Automatic W3C Validation Using YAML Conf in GitLab Continues Integration](https://ivanova-irina.blogspot.com.ee/2017/02/automatic-w3c-validation-using-yaml.html)
