var app = angular.module("blog", []);
var api = "https://www.googleapis.com/blogger/v3/blogs/4600357584726834841/posts?key=AIzaSyDnwRpxhIc-XHihHpe-AJFtAc8AndC2V-4&fields=items(published, url, title, content, labels)";

app.controller("posts", function ($scope, $http) {
  $http.get(api).then(function successCallback(response) {
    $scope.post = response.data.items;
  }, function errorCallback(response) {
    $scope.error = "Sorry, can't get last blog posts. " + response.status + " - " + response.data.error.message;
  });
});

app.filter("removeHTMLTags", function() {
  return function(text) {
    return text ? String(text).replace(/<[^>]+>/gm, "") : "";
  };
});
