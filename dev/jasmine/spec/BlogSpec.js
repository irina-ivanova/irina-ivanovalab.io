describe("Blog Posts", function() {
  var $controller;

  beforeEach(function() {
    module("blog");
    inject(function(_$controller_) {
      $controller = _$controller_;
    });
  });

  it("should return something", function() {

  });
});

describe("HTML Tags Remover", function() {
  var $filter;

  beforeEach(function() {
    module("blog");
    inject(function($injector) {
      $filter = $injector.get("$filter");
    });
  });

  it("should remove valid HTML tags", function() {
    expect($filter("removeHTMLTags")("<b>bold text</b>")).toEqual("bold text");
  });

  it("should remove invalid HTML tags", function() {
    expect($filter("removeHTMLTags")("<b>bold text")).toEqual("bold text");
  });

  it("should return text as it is if there is no HTML tags", function() {
    expect($filter("removeHTMLTags")("usual text")).toEqual("usual text");
  });

  it("should be case sensitive", function() {
    expect($filter("removeHTMLTags")("Camel Case")).toEqual("Camel Case");
  });

  it("shouldn't remove < character", function() {
    expect($filter("removeHTMLTags")("4 < 7")).toEqual("4 < 7");
  });

  it("shouldn't remove > character", function() {
    expect($filter("removeHTMLTags")("11 > 3")).toEqual("11 > 3");
  });

});
