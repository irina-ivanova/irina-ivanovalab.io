#!/bin/bash
# HTML minifier http://html-minifier.com

url="http://html-minifier.com/raw";
file="public/index.html";

result=$( curl -X POST -s --data-urlencode "input@$file" $url );

if [[ $result == "" ]]; then
  echo "ERROR: HTML file $file can't be minified";
  exit 1;
else
  echo "OK: HTML file $file is minified";
  echo $result > $file;
  exit 0;
fi
