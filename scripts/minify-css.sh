#!/bin/bash
# CSS minifier https://cssminifier.com

url="https://cssminifier.com/raw";
file="public/css/style.css";

result=$( curl -X POST -s --data-urlencode "input@$file" $url );

if [[ $result == "" ]]; then
  echo "ERROR: CSS file $file can't be minified";
  exit 1;
else
  echo "OK: CSS file $file is minified";
  echo $result > $file;
  exit 0;
fi
