#!/bin/bash
# JavaScript minifier https://javascript-minifier.com

url="https://javascript-minifier.com/raw";
file="public/js/blog.js";

result=$( curl -X POST -s --data-urlencode "input@$file" $url );

if [[ $result == "" ]]; then
  echo "ERROR: JS file $file can't be minified";
  exit 1;
else
  echo "OK: JS file $file is minified";
  echo $result > $file;
  exit 0;
fi
